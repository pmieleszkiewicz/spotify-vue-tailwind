module.exports = {
  purge: ['./index.html', './src/**/*.{vue,js,ts,jsx,tsx}'],
  darkMode: false, // or 'media' or 'class'
  theme: {
    extend: {
      colors: {
        'spotify-gray-100': '#3c3c3c',
        'spotify-gray-200': '#2c2c2c',
        'spotify-gray-300': '#404040',
        'spotify-gray-500': '#1e1e1e',
        'spotify-gray-700': '#191919',

        'spotify-scroll': '#535353',
        'spotify-scroll-active': '#b2b2b2',

        'transparent-black-50': 'rgba(0, 0, 0, 0.50)',
      },
      keyframes: {
        scale: {
          '0%, 50%, 100%': { transform: 'scale(1)' },
          '25%, 75%': { transform: 'scale(1.15)' },
        }
      },
      animation: {
        scale: 'scale 2s ease-in-out 5',
      }
    },
  },
  variants: {
    extend: {},
  },
  plugins: [],
}
