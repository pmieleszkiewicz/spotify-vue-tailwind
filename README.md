## Spotify, Messenger with Tailwind CSS

Recreating Spotify and Messenger UIs using Vue 3 (with Vue Router) and Tailwind CSS. Just to learn a little bit of Tailwind.

### Demo
Demo is available on [Heroku](https://pm-vue-tailwind.herokuapp.com/).
