import App from "@/messenger/App";
import Chat from "@/messenger/pages/Chat";

const name = 'messenger';

export default {
    path: `/${name}`,
    name: `${name}:index`,
    component: App,
    children: [
        {
            path: '',
            name: `${name}:chat`,
            component: Chat,
        },
    ]
}
