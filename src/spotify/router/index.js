import Browse from "@/spotify/pages/Browse";
import Radio from "@/spotify/pages/Radio";
import Library from "@/spotify/pages/Library";
import Playlist from "@/spotify/pages/Playlist";
import Home from "@/spotify/pages/Home";
import App from "@/spotify/App";

const name = 'spotify';

export default {
    path: `/${name}`,
    name: `${name}:index`,
    component: App,
    children: [
        {
            path: '',
            name: `${name}:home`,
            component: Home,
        },
        {
            path: 'browse/:type?',
            name: `${name}:browse`,
            props: (route) => ({type: route.params.type || 'genres'}),
            component: Browse
        },
        {
            path: 'radio',
            name: `${name}:radio`,
            component: Radio
        },
        {
            path: 'library/:id',
            name: `${name}:library`,
            component: Library
        },
        {
            path: 'playlist/:id',
            name: `${name}:playlist`,
            component: Playlist
        }
    ]
}
