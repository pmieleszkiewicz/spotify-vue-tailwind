import { createApp } from 'vue'
import App from './Main.vue'
import router from './router'
import './index.scss'

createApp(App).use(router).mount('#app')
