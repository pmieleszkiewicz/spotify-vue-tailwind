import {createRouter, createWebHistory} from 'vue-router';
import Home from '@/pages/Home';
import spotifyRoutes from '@/spotify/router';
import messengerRoutes from '@/messenger/router';

const routes = [
    {
        path: '/',
        name: 'home',
        component: Home
    },
    spotifyRoutes,
    messengerRoutes
]

const router = createRouter({
    history: createWebHistory(process.env.BASE_URL),
    routes
})

export default router
